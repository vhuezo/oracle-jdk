#!/bin/sh
docker build -t  oracle-jdk:8u131 -f Dockerfile.java8 .

if [ $? -ne 0 ]; then
echo "Error to build image"
exit 1
else
echo -e "\nImage built succesfully!"
fi
